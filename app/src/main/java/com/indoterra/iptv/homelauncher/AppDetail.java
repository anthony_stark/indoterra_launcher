package com.indoterra.iptv.homelauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by dikaputra on 5/4/17.
 */

public class AppDetail {
    CharSequence label;
    CharSequence name;
    Drawable icon;
}
